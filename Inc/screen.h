#include "lcd5110.h"

#ifndef SCREEN_FIELD_H_
#define SCREEN_FIELD_H_

class screen {
public:
  screen(LCD5110_display*);
  void init();
  void redraw();
  void putPixel(uint8_t, uint8_t);
  void clearPixel(uint8_t, uint8_t);
  void clear();
  void drawBitmap(uint8_t*);
private:
  bool buffMap[84][48];
  bool map[84][48];
  LCD5110_display* _lcd;

  void redrawByte(uint8_t, uint8_t);
};
#endif
